import sys
import os

import calculations

import numpy as np
import scipy
import skimage

from skimage import io
from skimage.viewer import ImageViewer
from skimage.viewer.canvastools import RectangleTool
from skimage.viewer.canvastools import LineTool

import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUiType
Ui_MainWindow, QMainWindow = loadUiType('complianceMainUI.ui')



# The loadUiType function requires a single argument, the name of a Designer UI file, 
# and returns a tuple of the appropriate application classes. The return tuple contains
# two class definitions.

# The first is our custom GUI application class, set to the Ui_MainWindow variable. 
# The second is the base class for our custom GUI application, which in this case 
# is PyQt4.QtGui.QMainWindow. The base class type is defined by the widget we first
# created with Designer. Note: These are not instances of these classes, but the 
# class definitions themselves. They are meant to serve as superclasses to our new 
# application logic class.

# NOTE: This usage differs from many other examples. Tutorials typically convert the
# UI file to a Python module using the pyuic5 utility, and then import that module 
# into a new script, i.e. from window import Ui_MainWindow. The downside of this 
# approach is that, in addition to generating an extra file, others might be tempted
# to modify the Python module directly. If the application design is changes, which
# updates the UI file, then the pyuic5 conversion will destroy any custom modifications 
# to the Python module. By using the UI file directly, as I’ve done above, it will not 
# be possible to inadvertently alter the GUI design code.


class MainClass(QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):

        # inherit from CoreSeg designer output
        super(MainClass, self).__init__(parent)
        # This is defined in design.py file automatically
        # It sets up layout and widgets that are defined
        self.setupUi(self)

        self.btn_openDirectory.clicked.connect(self._browse_to_folder)
        self.btn_rms.clicked.connect(self._activate_rect_draw)
        self.btn_calculateRMS.clicked.connect(self._get_rms)
        self.btn_lineProfile.clicked.connect(self._activate_line_draw)
        self.btn_calculateLineProfile.clicked.connect(self._get_profile)
        self.btn_calculateWidth.clicked.connect(self._get_sample_width)
        self.btn_nextSample.clicked.connect(self._next_sample)
        self.btn_previousSample.clicked.connect(self._previous_sample)
        self.btn_save.clicked.connect(self._save_data)
        
        
        
    def _browse_to_folder(self):

        self.openFileDialog = QtWidgets.QFileDialog(self)
        # set the mode of the file dialog to only select directories
        self.openFileDialog.setFileMode(self.openFileDialog.DirectoryOnly)
        self.openFileDialog.show()

        if (self.openFileDialog.exec()):
            directoryList = self.openFileDialog.selectedFiles()
            self.directoryName = directoryList[0]
            

            # display the directory name in the lineEdit_import widget
            self.lineEdit_directory.setText(self.directoryName)

            # create a list of the paths to all samples/trials in the given experiment directory
            # and place is self.trial_paths
            self._list_files()
            
            # readin the first trial image and display
            self.image_count = 0
            # count total number of trials in the experiment
            self.total_images = len(self.trial_paths)
            
            self._readin_image()
            
        if self.total_images > 1:
            self.btn_nextSample.setEnabled(True)

		
    def _list_files(self):
        
        self.samples = os.listdir(self.directoryName)
        
        self.num_trials_per_sample = {}
        self.count_to_sample = {}
        self.count_to_trial = {}
        counter = 0
        
        self.data = {}
        self.trial_paths = []

        for sample in self.samples:  
            sample_path = os.path.join(self.directoryName, sample)            
            trials = os.listdir(sample_path)
            
            self.num_trials_per_sample[sample] = len(trials)
            self.data[sample] = np.zeros(shape=(3, self.num_trials_per_sample[sample]))
            
            for index, trial in enumerate(trials):
                self.trial_paths.append(os.path.join(sample_path, trial))
                self.count_to_sample[counter] = sample
                self.count_to_trial[counter] = index
                counter += 1
    
    def _readin_image(self):
        
        self.display_rgb_image = io.imread(self.trial_paths[self.image_count])
        self.display_image = skimage.color.rgb2gray(self.display_rgb_image)
       
        self.display_viewer = ImageViewer(self.display_image)        
        # a vertical layout was enforced on a dummy widget embedded inside the 
        # widget_photo in order to hack this little layout addwiget function together
        self.widget_photo_vl.addWidget(self.display_viewer)
        self.display_viewer.show()
        
        self.spinBox_threshold.setEnabled(True)
        self.btn_rms.setEnabled(True)

    def _activate_rect_draw(self):
        
        self.rect_tool = RectangleTool(self.display_viewer)
        self.btn_calculateRMS.setEnabled(True)
        
    def _get_rms(self):
    
        self.rms_minval = calculations.rms(image=self.display_image,
                                           extents=self.rect_tool.extents, 
                                           threshold=self.spinBox_threshold.value())
        self.btn_lineProfile.setEnabled(True)
        
        self.rms_minval_array = np.zeros(shape=self.display_image.shape[1])
        self.rms_minval_array.fill(self.rms_minval)
        
        self._create_profile_fig(call_from='rms')
                                           
    def _activate_line_draw(self):
    
        self.line_tool = LineTool(self.display_viewer)
        self.btn_calculateLineProfile.setEnabled(True)
        
    def _get_profile(self):
    
        self.full_profile = calculations.full_profile(image=self.display_image,
                                                      end_points=self.line_tool.end_points)
                                                 
        self._create_profile_fig(call_from='profile')
        
        self.btn_calculateWidth.setEnabled(True)
                                                 
        
    def _display_profiles(self, fig):
        
        # figure canvas widget instance contains the 'fig' plot
        self.canvas = FigureCanvas(fig)
        self.widget_lineProfile_vl.addWidget(self.canvas)
        self.canvas.draw()
        
    def _create_profile_fig(self, call_from):
        
        if call_from == 'rms':
        
            fig1 = Figure()
            ax1f1 = fig1.add_subplot(111)
            ax1f1.plot(self.rms_minval_array)
            self._display_profiles(fig1)
            
        elif call_from == 'profile':
            
            self._close_profiles()
            
            fig1 = Figure()
            ax1f1 = fig1.add_subplot(111)
            ax1f1.plot(self.rms_minval_array)
            ax1f1.plot(self.full_profile)
            self._display_profiles(fig1)
        
    def _close_profiles(self):
        
        self.widget_lineProfile_vl.removeWidget(self.canvas)
        self.canvas.close()
        
    def _close_display_viewer(self):
    
        self.widget_photo_vl.removeWidget(self.display_viewer)
        self.display_viewer.close()
        
    def _close_all(self):
        
        try:
            self._close_profiles()
            self._close_display_viewer()
            
        except AttributeError:
            self._close_display_viewer()

        
    def _get_sample_width(self):
        
        self.sample_widths = calculations.sample_width_from_profiles(line_profile=self.full_profile,
                                                                     minval=self.rms_minval)
        
        self.data[self.count_to_sample[self.image_count]][:, self.count_to_trial[self.image_count]] = self.sample_widths
        
        self.btn_save.setEnabled(True)
        
    def _save_data(self):
        
        for sample in self.data:
            sample_name = sample + '.csv'
            file_name = os.path.join(self.directoryName, sample_name)
            np.savetxt(file_name, self.data[sample], 
                       delimiter=',', newline='\n')
        
        
        
    def _next_sample(self):
        
        self._close_all()
        
        if self.image_count + 1 >= self.total_images - 1:
            
            self.btn_nextSample.setEnabled(False)
            self.spinBox_threshold.setEnabled(False)
            self.btn_rms.setEnabled(False)
            self.btn_calculateRMS.setEnabled(False)
            self.btn_lineProfile.setEnabled(False)
            self.btn_calculateLineProfile.setEnabled(False)
            self.btn_calculateWidth.setEnabled(False)
            
            self.btn_previousSample.setEnabled(True)
            
            self.image_count += 1
            self._readin_image()
            
            
        else:
            
            self.spinBox_threshold.setEnabled(False)
            self.btn_rms.setEnabled(False)
            self.btn_calculateRMS.setEnabled(False)
            self.btn_lineProfile.setEnabled(False)
            self.btn_calculateLineProfile.setEnabled(False)
            self.btn_calculateWidth.setEnabled(False)
            
            self.btn_previousSample.setEnabled(True)
            
            self.image_count += 1
            self._readin_image()
            
    def _previous_sample(self):
        
        self._close_all()
                
        if self.image_count <= 1:
            
            self.btn_previousSample.setEnabled(False)
            self.spinBox_threshold.setEnabled(False)
            self.btn_rms.setEnabled(False)
            self.btn_calculateRMS.setEnabled(False)
            self.btn_lineProfile.setEnabled(False)
            self.btn_calculateLineProfile.setEnabled(False)
            self.btn_calculateWidth.setEnabled(False)
            
            self.image_count -= 1
            self._readin_image()
            
            
        else:
            
            self.btn_nextSample.setEnabled(True)
            self.spinBox_threshold.setEnabled(False)
            self.btn_rms.setEnabled(False)
            self.btn_calculateRMS.setEnabled(False)
            self.btn_lineProfile.setEnabled(False)
            self.btn_calculateLineProfile.setEnabled(False)
            self.btn_calculateWidth.setEnabled(False)
            
            self.image_count -= 1
            self._readin_image()
            
            
            
            
        

def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainClass()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()