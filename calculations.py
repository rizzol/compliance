import os
import sys

import numpy as np
import scipy


def rms(image, extents, threshold):
    
    r_xmin = int(np.round(extents[0]))
    r_xmax = int(np.round(extents[1]))
    r_ymin = int(np.round(extents[2]))
    r_ymax = int(np.round(extents[3]))
    
    rms = np.sqrt(np.mean(image[r_ymin:r_ymax, r_xmin:r_xmax]))
    minval = threshold * rms
    
    return minval
    
def full_profile(image, end_points):

    l_x1 = end_points[0][0]
    l_x2 = end_points[1][0]
    l_xmin = min(l_x1, l_x2)
    l_xmax = max(l_x1, l_x2)
    l_y = end_points[0][1]

    # create a zerod array and fill with image values along line profile
    # to create a plot which matches the total image width
    line_profile = np.zeros(shape=image.shape[1])
    line_profile[l_xmin:l_xmax] = image[l_y, l_xmin:l_xmax]
    
    return line_profile
    
def sample_width_from_profiles(line_profile, minval):
    
    line_profile[line_profile < minval] = 0
    line_profile[np.nonzero(line_profile)] = 1
    
    labeled = scipy.ndimage.label(line_profile)
    
    sample1_pixels = 0
    sample2_pixels = 0
    sample3_pixels = 0

    for pixel in labeled[0]:
        if pixel == 1:
            sample1_pixels += 1
            
        elif pixel == 2:
            sample2_pixels += 1
            
        elif pixel ==3:
            sample3_pixels += 1
            
        else:
            continue
            
    samples = np.array([sample1_pixels, sample2_pixels, sample3_pixels])
               
    return samples