import os
import sys
import time


def startup_check():

    if not os.path.isdir('/home/pi/compliance_data/'):
        os.mkdir('/home/pi/compliance_data/')
    return


def focus_camera():
    # execute open webcame video feed in a seperate subshell 

    os.system('luvcview')
    return


def create_experiment_directory():

    experiment_name_valid = False

    while experiment_name_valid == False:
        experiment_name = input('\nplease provide an experiment name: ')
        output_directory = os.path.join('/home/pi/compliance_data', experiment_name)
        
        if os.path.isdir(output_directory):
            print('That experiment name already exists.')
            print('please use a different name or delete the directory ', output_directory, '\n')
        
        else:
            experiment_name_valid = True
            os.mkdir(output_directory)
            print('thank you, output files will be saved in ', output_directory)

    return output_directory


def create_sample_directory(experiment_directory):
    
    sample_name_valid = False

    while sample_name_valid == False:
        sample_name = input('\ninput sample id/name: ')
        sample_directory = os.path.join(experiment_directory, sample_name)
        
        if os.path.isdir(sample_directory):
            print('The same sample name already exists within the given experiment.')
            print('please use a different name or delete the directory ', sample_directory)
        
        else:
            sample_name_valid = True
            os.mkdir(sample_directory)
            print('thank you, sample files will be saved in ', sample_directory)   
            print('-------------------------------------')
            print('- beginning trials for sample: ', sample_name)
            print('-------------------------------------')
    
    return sample_directory


def get_trial_name(sample_directory):

    trial_name_valid = False
    while trial_name_valid == False:
        trial_name = input('\nInput Trial id/name: ')
        trial_path = os.path.join(sample_directory, trial_name)

        if os.path.isfile(trial_path):
            print('That trial name already exists for the current sample.')
            print('Please use a different trial name or delete the file ', trial_path)

        else:
            trial_name_valid = True
            print('The current sample is: ', os.path.split(sample_directory)[1])
            print('The current trial is: ', trial_name)

    return trial_path


def perform_measurments(experiment_directory):

    do_sample = True

    while do_sample:
        do_trial = True
        sample_directory = create_sample_directory(experiment_directory)

        while do_trial:
            trial_path = get_trial_name(sample_directory) 
            
            webcam_bash_call = 'fswebcam -r 1920x1080 --no-banner' + ' ' + trial_path + '.jpg'
            print(webcam_bash_call)
            input('\n ** press any key to perform capture... ** \n')
            os.system(webcam_bash_call) 
            
            if input('\nwould you like to perform another trial? y/n: ') == 'n':
                do_trial = False
        
        if input('\nwould you like to acquire another sample? y/n: ') == 'n':
            do_sample = False
    
    
if __name__ == "__main__":

    startup_check()

    print('Welcome to the Data Collection Module')
    print('')
    print('-------------------------------------')
    print('')

    check_focused = input('Is the camera focused? Enter y/n: ')

    if check_focused == 'y':
        print('Since the camera is focused, we will now begin data collection')

    elif check_focused == 'n':
        print('Opening the camera focus screen.')
        print('Please exit the window when the camera is focused')
        time.sleep(1)

        focus_camera()

        print('\n The camera is now focused, proceeding to next step')

    else:
        print('command not recognized')
        quit()     
    
    experiment_directory = create_experiment_directory() 
    
    perform_measurments(experiment_directory)

    print('-------------------------------------')
    print('- End of measurments... Goodbye!     ')
    print('-------------------------------------')
